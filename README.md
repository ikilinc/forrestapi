projet FORREST par DAVID Aurélien, KILINC Ismail, PATRAS Quentin                                   


Le projet Forum est divisée en 3 parties : 
- Le client riche (front) qui est dans un répertoire git différent de celui-ci : [ForrestFront](http://gitlab.com/ikilinc/forrestfront)
- l'api qui se trouve dans ce répertoire git
- la base de données, un dump forrest.sql permet d'importer la structure et les données dans la base forrest une fois créée


Cet api joue le rôle de contrôler-serveur, il est directement connecté à la base de données pour effectuer les différentes méthodes (afficher, modifier, créer et supprimer) sur les données stockées en base.

/--------------INSTALLATIONS-------------/


[INSTALLATION FRONT] 

Dans le dossier www ou htdocs, effectuer les commandes suivantes : 

git clone http://gitlab.com/ikilinc/forrestfront

Un dossier forrestfront sera créé et l'interface sera visible avec l'url suivant : http://localhost/forrestfront

[INSTALLATION API] 

Dans le dossier www ou htdocs, effectuer les commandes suivantes : 

git clone http://gitlab.com/ikilinc/forrestapi

cd forrestapi

composer update

Activer sous xampp ou wampp les serveurs Apache et MySQL

Dans le navigateur, tapez l'url http://localhost/phpmyadmin et créez la base de données nommée forrest et importer le fichier forrest.sql

(pour Linux)

Dans le fichier .env, renseignez l'adresse du serveur de la base de données et importez dans celle ci le fichier forrest.sql après avoir créé la base.

Accéder au fichier config/services.yaml

Décommenter la partie Linux et commenter la partie Windows

php -S localhost:8000 (dans forrestfront, les requêtes vers l'api se feront vers cet url et ce numéro de port particulièrement)

Accès : http://localhost/forresfront

login modérateur : roza@forrest.fr mot de passe : roza

login utilisateur : utilisateur@forrest.fr mot de passe : test

------------------------------------------------------------------------------------------------------------------------------------------

------ Réalisation de l'API : -----

L'API a été réalisée avec le framework symfony.

Création du projet symfony "forrestapi" et initialisation : 
    
    1-	Installation du composer (Windows) : 
        a.	Télécharger le setup de composer en cliquant sur le lien suivant : 
 	            https://getcomposer.org/Composer-Setup.exe
        b.	Démarrer l’installateur puis sélectionner le php.exe se trouvant dans le dossier php de WAMPP ou XAMPP 
 	            (ex : C:\xampp\php\php.exe)
        c.	Cliquer sur suivant et finaliser l’installation.

    2-	Creation du projet Symfony :
        a.	Ouvrir un terminal de commande dans le dossier htdocs\forrest de XAMPP ou www\forrest de WAMPP 
        b.	Dans celui-ci, taper la commande suivante : 
            composer create-project symony-skeleton forrestapi
    
Une fois le projet créé les dépendances suivantes sont requises : 
    
    1- Annotations pour les routes : 
        composer require annotations
        
    2- Doctrine-ORM pour communiquer avec la base de données et manipuler les données :
        composer require symfony/orm-pack
    
    3- Bundle additionnels : 
        composer require --dev symfony/maker-bundle
        composer require symfony/web-server-bundle –dev


[Base de données]


La base de données utilisée est dans la bdd MySQL fournie par Xampp.

dans le dossier root du projet symfony forrestapi, le fichier .env contient les paramètres pour la connexion à la base de données.

Il est nécessaire de renseigner l'url de la base de données utilisée 

ex : DATABASE_URL=mysql://root@127.0.0.1:3306/forrest?serverVersion=10.1.30-MariaDB

Après avoir activé Apache et MySQL dans Xampp, nous pouvons accéder au sgbd dans un navigateur avec l'url suivant : 
http://localhost/phpmyadmin

Par défaut, le login utilisateur est root, sans mot de passe.

Nous avons créé une database forrest qui contient 6 tables : 

- Sujet, pour stocker tous les sujets, toutes les questions posées dans le forum
- Categorie, pour stocker les categories des sujets 
- Commentaire, pour stocker les commentaires sur les différents sujets du forum
- Etat, pour stocker les états des sujets (actif, inactif)
- Utilisateur, pour permettre l'authentification des utilisateurs et stocker leurs données
- Role, pour attribuer différents rôles aux utilisateurs (modérateur, utilisateur)

Le schéma relationnel (MCD.png) est présent dans ce répertoire git.

Chaque table de la base de données est associée à une entité dans l'api Symfony : 

(maker-bundle) : php bin/console make:entity

et chaque entité possède un contrôleur. C'est dans ce contrôleur que nous retrouvons toutes les manipulations des données concernées (CRUD)

(maker-bundle) : php bin/console make:controller

Dans ces contrôleurs, les manipulations sont effectuées en fonction des routes (annotations).

La liste des routes créées est présente dans le fichier "Liste des Routes API Forrest.docx"

