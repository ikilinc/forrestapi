<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Sujet;
use App\Entity\Etat;
use App\Entity\Categorie;
use App\Entity\Utilisateur;
use App\Entity\Commentaire;
use \DateTime;

class SujetController extends AbstractController
{
    /**
    * @Route("/sujets", name="liste_sujets", methods={"GET"})
    */
    public function listeSujets()
    {
        $repository   = $this->getDoctrine()->getRepository(Sujet::class);
        $listeSujets  = $repository->findAll();
        $listeReponse = array();

        if($listeSujets) {
            foreach ($listeSujets as $sujet) {
                $listeCommentaires = $sujet->getCommentaires();
                $com = null;
                foreach ($listeCommentaires as $commentaire) {
                    $mostRecent = new DateTime('2000-01-01');
                    $curDate = $commentaire->getDateCom();
                    if ($curDate > $mostRecent) {
                        $mostRecent = $curDate;
                        $com = $commentaire;
                    }
                }
                $commentaireRecent = "Aucun commentaire...";
                if($com) {
                    $commentaireRecent = array(
                        "id"    => $com->getId(),
                        "contenu"    => base64_decode($com->getContenu()),
                        "date"    => $com->getDateCom()->format('Y-m-d H:i:s'),
                        "note"    => $com->getNote(),
                        "auteur"    => $com->getAuteur()->getPseudo(),
                        "sujet"    => $com->getSujet()->getId(),
                    );
                }

                $listeReponse[] = array(
                    'id'     => $sujet->getId(),
                    'titre'    => base64_decode($sujet->getTitre()),
                    'description' => base64_decode($sujet->getDescription()),
                    'creation_date' => $sujet->getDateCreation()->format('Y-m-d H:i:s'),
                    'etat' => array(
                        "id" => $sujet->getEtat()->getId(),
                        "libelle" => $sujet->getEtat()->getLibelle(),
                    ),
                    'auteur' => array(
                        "id" => $sujet->getAuteur()->getId(),
                        "pseudo" => $sujet->getAuteur()->getPseudo(),
                    ),
                    'categorie' => array(
                        "id" => $sujet->getCategorie()->getId(),
                        "libelle" => $sujet->getCategorie()->getLibelle(),
                    ),
                    'commentaire-recent' => $commentaireRecent,
                );
            }
        } else { 
            $listeReponse[] = array(
                "erreur" => "Erreur lors de la recherche !"
            );
        }
        $reponse = new Response();
        $reponse->setContent(json_encode(array("sujets"=>$listeReponse)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/sujet/{id}", name="details_sujet", methods={"GET"})
    */
    public function detailsSujet($id)
    {
        $repository = $this->getDoctrine()->getRepository(Sujet::class);
        $sujet     = $repository->find($id);
        
        if($sujet) {
            $listeCommentaires = $sujet->getCommentaires();
            $commentaires = "Aucun commentaire...";
            if($listeCommentaires) {
                $commentaires = array();
                foreach ($listeCommentaires as $commentaire) {
                    $commentaires[] = array(
                        "id"    => $commentaire->getId(),
                        "contenu"    => base64_decode($commentaire->getContenu()),
                        "date"    => $commentaire->getDateCom()->format('Y-m-d H:i:s'),
                        "note"    => $commentaire->getNote(),
                        "auteur"    => $commentaire->getAuteur()->getPseudo(),
                        "sujet"    => $commentaire->getSujet()->getId(),
                    );
                }
            }
            $listeReponse = array(
                'id'     => $sujet->getId(),
                'titre'    => base64_decode($sujet->getTitre()),
                'description' => base64_decode($sujet->getDescription()),
                'creation_date' => $sujet->getDateCreation()->format('Y-m-d H:i:s'),
                'etat' => array(
                    "id" => $sujet->getEtat()->getId(),
                    "libelle" => $sujet->getEtat()->getLibelle(),
                ),
                'auteur' => array(
                    "id" => $sujet->getAuteur()->getId(),
                    "pseudo" => $sujet->getAuteur()->getPseudo(),
                ),
                'categorie' => array(
                    "id" => $sujet->getCategorie()->getId(),
                    "libelle" => $sujet->getCategorie()->getLibelle(),
                ),
                'commentaires' => $commentaires,
            );
        } else {
            $listeReponse[] = "Aucun sujet portant l'id ".$id." !";
        }
        $reponse = new Response();
        $reponse->setContent(json_encode(array("sujet"=>$listeReponse)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/sujets/auteur/{auteurId}", name="details_sujets_auteur", methods={"GET"})
    */
    public function detailsSujetsAuteur($auteurId)
    {
        $repository = $this->getDoctrine()->getRepository(Utilisateur::class);
        $auteur     = $repository->find($auteurId);
        $listeSujets = null;
        if($auteur) {
            $listeSujets = $auteur->getSujets();
            
            $sujets = array();
            foreach ($listeSujets as $sujet) {
                
                $listeCommentaires = $sujet->getCommentaires();
                $com = null;
                foreach ($listeCommentaires as $commentaire) {
                    $mostRecent = new DateTime('2000-01-01');
                    $curDate = $commentaire->getDateCom();
                    if ($curDate > $mostRecent) {
                        $mostRecent = $curDate;
                        $com = $commentaire;
                    }
                }
                $commentaireRecent = "Aucun commentaire...";
                if($com) {
                    $commentaireRecent = array(
                        "id"    => $com->getId(),
                        "contenu"    => base64_decode($com->getContenu()),
                        "date"    => $com->getDateCom()->format('Y-m-d H:i:s'),
                        "note"    => $com->getNote(),
                        "auteur"    => $com->getAuteur()->getPseudo(),
                        "sujet"    => $com->getSujet()->getId(),
                    );
                }

                $sujets[] = array(
                    'id'     => $sujet->getId(),
                    'titre'    => base64_decode($sujet->getTitre()),
                    'description' => base64_decode($sujet->getDescription()),
                    'creation_date' => $sujet->getDateCreation()->format('Y-m-d H:i:s'),
                    'etat' => array(
                        "id" => $sujet->getEtat()->getId(),
                        "libelle" => $sujet->getEtat()->getLibelle(),
                    ),
                    'auteur' => array(
                        "id" => $sujet->getAuteur()->getId(),
                        "pseudo" => $sujet->getAuteur()->getPseudo(),
                    ),
                    'categorie' => array(
                        "id" => $sujet->getCategorie()->getId(),
                        "libelle" => $sujet->getCategorie()->getLibelle(),
                    ),
                    'commentaire-recent' => $commentaireRecent,
                );
            }

            $reponse = new Response(json_encode(array(
                'id'     => $auteur->getId(),
                'libelle'    => $auteur->getPseudo(),
                'sujets' => $sujets,
            )));
        } else {
            $reponse = new Response(json_encode(array(
                "erreur" => "Auteur portant l'id ".$auteurId." inexistant !")));
        }
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/sujets/categorie/{categorieId}", name="details_sujets_categorie", methods={"GET"})
    */
    public function detailsSujetsCategorie($categorieId)
    {
        $repository = $this->getDoctrine()->getRepository(Categorie::class);
        $categorie     = $repository->find($categorieId);
        $listeSujets = null;
        if($categorie) {
            $listeSujets = $categorie->getSujets();
        
            $sujets = array();
            foreach ($listeSujets as $sujet) {
                
                $listeCommentaires = $sujet->getCommentaires();
                $com = null;
                $commentaires = array();
                foreach ($listeCommentaires as $commentaire) {
                    $mostRecent = new DateTime('2000-01-01');
                    $curDate = $commentaire->getDateCom();
                    if ($curDate > $mostRecent) {
                        $mostRecent = $curDate;
                        $com = $commentaire;
                    }
                    $commentaires[] = array(
                        "id"    => $commentaire->getId(),
                        "contenu"    => base64_decode($commentaire->getContenu()),
                        "date"    => $commentaire->getDateCom()->format('Y-m-d H:i:s'),
                        "note"    => $commentaire->getNote(),
                        "auteur"    => $commentaire->getAuteur()->getPseudo(),
                        "sujet"    => $commentaire->getSujet()->getId(),
                    );
                }
                $commentaireRecent = "Aucun commentaire...";
                if($com) {
                    $commentaireRecent = array(
                        "id"    => $com->getId(),
                        "contenu"    => base64_decode($com->getContenu()),
                        "date"    => $com->getDateCom()->format('Y-m-d H:i:s'),
                        "note"    => $com->getNote(),
                        "auteur"    => $com->getAuteur()->getPseudo(),
                        "sujet"    => $com->getSujet()->getId(),
                    );
                }
                if(empty($commentaires)) $commentaires = $commentaireRecent;
                $sujets[] = array(
                    'id'     => $sujet->getId(),
                    'titre'    => base64_decode($sujet->getTitre()),
                    'description' => base64_decode($sujet->getDescription()),
                    'creation_date' => $sujet->getDateCreation()->format('Y-m-d H:i:s'),
                    'etat' => array(
                        "id" => $sujet->getEtat()->getId(),
                        "libelle" => $sujet->getEtat()->getLibelle(),
                    ),
                    'auteur' => array(
                        "id" => $sujet->getAuteur()->getId(),
                        "pseudo" => $sujet->getAuteur()->getPseudo(),
                    ),
                    'categorie' => array(
                        "id" => $sujet->getCategorie()->getId(),
                        "libelle" => $sujet->getCategorie()->getLibelle(),
                    ),
                    'commentaire-recent' => $commentaireRecent,
                    'commentaires' => $commentaires,
                );
            }
            
            $reponse = new Response(json_encode(array(
                'id'     => $categorie->getId(),
                'libelle'    => $categorie->getLibelle(),
                'sujets' => $sujets,
            )));
        } else {
            $reponse = new Response(json_encode(array(
                "erreur" => "Catégorie portant l'id ".$categorieId." inexistante !")));
        }
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/sujet/new/{titre}/{description}/{auteurId}/{categorieId}", name="nouveau_sujet", methods={"POST"})
    */
    public function nouveauSujet($titre, $description, $auteurId, $categorieId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $sujet = new Sujet();
        $sujet->setTitre($titre);
        $sujet->setDescription($description);
        $sujet->setDateCreation(new DateTime("now"));

        $repositoryEtat   = $this->getDoctrine()->getRepository(Etat::class);
        $etat  = $repositoryEtat->find(1);
        $sujet->setEtat($etat);

        $repositoryUtilisateur   = $this->getDoctrine()->getRepository(Utilisateur::class);
        $auteur  = $repositoryUtilisateur->find($auteurId);
        $sujet->setAuteur($auteur);

        $repositoryCategorie   = $this->getDoctrine()->getRepository(Categorie::class);
        $categorie  = $repositoryCategorie->find($categorieId);
        $sujet->setCategorie($categorie);

        $entityManager->persist($sujet);
        $entityManager->flush();

        $reponse = new Response(json_encode(array(
            'id'     => $sujet->getId(),
            'titre'    => base64_decode($sujet->getTitre()),
            'description' => base64_decode($sujet->getDescription()),
            'creation_date' => $sujet->getDateCreation()->format('Y-m-d H:i:s'),
            'etat' => $sujet->getEtat()->getId(),
            'auteur' => $sujet->getAuteur()->getId(),
            'categorie' => $sujet->getCategorie()->getLibelle(),
            )
        ));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/sujet/delete/{id}", name="suppression_sujet", methods={"DELETE"})
    */
    public function suppressionSujet($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Sujet::class);
        $sujet     = $repository->find($id);
        $entityManager->remove($sujet);
        $entityManager->flush();
        $reponse = new Response(json_encode(array(
            'id'     => $sujet->getId(),
            'titre'    => base64_decode($sujet->getTitre()),
            'auteur' => $sujet->getAuteur()->getId(),
            ))
        );
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/sujet/modif/{id}/{titre}/{description}/{auteurId}/{categorieId}/{etatId}", name="modification_sujet", methods={"PUT"})
    */
    public function modificationSujet($id, $titre, $description, $auteurId, $categorieId, $etatId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Sujet::class);
        $sujet        = $repository->find($id);
        $sujet->setTitre($titre);
        $sujet->setDescription($description);
        $sujet->setDateCreation(new DateTime("now"));

        $repositoryEtat   = $this->getDoctrine()->getRepository(Etat::class);
        $etat  = $repositoryCategorie->find($etatId);
        $sujet->setEtat($etat);

        $repositoryUtilisateur   = $this->getDoctrine()->getRepository(Utilisateur::class);
        $auteur  = $repositoryUtilisateur->find($auteurId);
        $sujet->setAuteur($auteur);

        $repositoryCategorie   = $this->getDoctrine()->getRepository(Categorie::class);
        $categorie  = $repositoryCategorie->find($categorieId);
        $sujet->setCategorie($categorie);

        $entityManager->persist($sujet);
        $entityManager->flush();

        $reponse = new Response(json_encode(array(
            'id'     => $sujet->getId(),
            'titre'    => base64_decode($sujet->getTitre()),
            'description' => base64_decode($sujet->getDescription()),
            'creation_date' => $sujet->getDateCreation()->format('Y-m-d H:i:s'),
            'etat' => array(
                "id" => $sujet->getEtat()->getId(),
                "libelle" => $sujet->getEtat()->getLibelle(),
            ),
            'auteur' => array(
                "id" => $sujet->getAuteur()->getId(),
                "pseudo" => $sujet->getAuteur()->getPseudo(),
            ),
            'categorie' => array(
                "id" => $sujet->getCategorie()->getId(),
                "libelle" => $sujet->getCategorie()->getLibelle(),
            ),
        )));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }
}
