<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Sujet;
use App\Entity\Etat;
use App\Entity\Categorie;
use App\Entity\Utilisateur;
use App\Entity\Commentaire;
use \DateTime;


class CommentaireController extends AbstractController
{
    /**
    * @Route("/commentaires", name="liste_commentaires", methods={"GET"})
    */
    public function listeCommentaires()
    {
        $repository   = $this->getDoctrine()->getRepository(Commentaire::class);
        $listeCommentaires = $repository->findAll();
        $listeReponse = array();
        
        if($listeCommentaires) {

            foreach ($listeCommentaires as $commentaire) {
                $listeReponse[] = array(
                    'id'     => $commentaire->getId(),
                    'contenu'    => base64_decode($commentaire->getContenu()),
                    'date_com' => $commentaire->getDateCom()->format('Y-m-d H:i:s'),
                    'note' => $commentaire->getNote(),
                    'auteur' => array(
                        "id" => $commentaire->getAuteur()->getId(),
                        "pseudo" => $commentaire->getAuteur()->getPseudo(),
                    ),
                    'sujet' => array(
                        "id" => $commentaire->getSujet()->getId(),
                        "titre" => $commentaire->getSujet()->getTitre(),
                    ),
                );
            }
        } else {
            $listeReponse = "Aucun commentaire ...";
        }
        $reponse = new Response();
        $reponse->setContent(json_encode(array("commentaires"=>$listeReponse)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/commentaire/{id}", name="details_commentaire", methods={"GET"})
    */
    public function detailsCommentaire($id)
    {
        $repository = $this->getDoctrine()->getRepository(Commentaire::class);
        $commentaire     = $repository->find($id);

        $listeReponse = array();
        $reponse = new Response();

        if($commentaire) {
            $listeReponse[] = array(
                'id'     => $commentaire->getId(),
                'contenu'    => base64_decode($commentaire->getContenu()),
                'date_com' => $commentaire->getDateCom()->format('Y-m-d H:i:s'),
                'note' => $commentaire->getNote(),
                'auteur' => array(
                    "id" => $commentaire->getAuteur()->getId(),
                    "pseudo" => $commentaire->getAuteur()->getPseudo(),
                ),
                'sujet' => array(
                    "id" => $commentaire->getSujet()->getId(),
                    "titre" => $commentaire->getSujet()->getTitre(),
                ),
            );
        } else {
             $listeReponse = "Aucun commentaire correspondant !";
        }

        $reponse->setContent(json_encode(array("commentaire"=>$listeReponse)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/commentaires/sujet/{sujetId}", name="details_commentaires_sujet", methods={"GET"})
    */
    public function detailsCommentairesSujet($sujetId)
    {
        $repository = $this->getDoctrine()->getRepository(Sujet::class);
        $sujet     = $repository->find($sujetId);
        $listeCommentaires = null;
        if($sujet) {
            $listeCommentaires = $sujet->getCommentaires();

            $commentaires = array();
            foreach ($listeCommentaires as $commentaire) {
                $commentaires[] = array(
                    'id'     => $commentaire->getId(),
                    'contenu'    => base64_decode($commentaire->getContenu()),
                    'date_com' => $commentaire->getDateCom()->format('Y-m-d H:i:s'),
                    'note' => $commentaire->getNote(),
                    'auteur' => array(
                        "id" => $commentaire->getAuteur()->getId(),
                        "pseudo" => $commentaire->getAuteur()->getPseudo(),
                    ),
                    'sujet' => array(
                        "id" => $commentaire->getSujet()->getId(),
                        "titre" => $commentaire->getSujet()->getTitre(),
                    ),
                );
            }
            if(empty($commentaires)) {
                $commentaires = "Aucun commentaire pour ce sujet !";
            }

            $reponse = new Response(json_encode(array(
                'id'     => $sujet->getId(),
                'titre'    => $sujet->getTitre(),
                'commentaires' => $commentaires,
            )));
        } else {
            $reponse = new Response(json_encode(array(
                "erreur" => "Sujet portant l'id ".$sujet." inexistant !")));
        }
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/commentaires/auteur/{auteurId}", name="details_commentaires_auteur", methods={"GET"})
    */
    public function detailsCommentairesAuteur($auteurId)
    {
        $repository = $this->getDoctrine()->getRepository(Utilisateur::class);
        $auteur     = $repository->find($auteurId);
        $listeCommentaires = null;
        if($auteur) {
            $listeCommentaires = $auteur->getCommentaires();

            $commentaires = "Aucun commentaire de cet auteur !";
            $commentaires = array();
            foreach ($listeCommentaires as $commentaire) {
                $commentaires[] = array(
                    'id'     => $commentaire->getId(),
                    'contenu'    => base64_decode($commentaire->getContenu()),
                    'date_com' => $commentaire->getDateCom()->format('Y-m-d H:i:s'),
                    'note' => $commentaire->getNote(),
                    'auteur' => array(
                        "id" => $commentaire->getAuteur()->getId(),
                        "pseudo" => $commentaire->getAuteur()->getPseudo(),
                    ),
                    'sujet' => array(
                        "id" => $commentaire->getSujet()->getId(),
                        "titre" => $commentaire->getSujet()->getTitre(),
                    ),
                );
            }

            $reponse = new Response(json_encode(array(
                'id'     => $auteur->getId(),
                'auteur'    => $auteur->getPseudo(),
                'commentaires' => $commentaires,
            )));
        } else {
            $reponse = new Response(json_encode(array(
                "erreur" => "Auteur portant l'id ".$auteurId." inexistant !")));
        }
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/commentaire/new/{contenu}/{auteurId}/{sujetId}", name="nouveau_commentaire", methods={"POST"})
    */
    public function nouveauCommentaire($contenu, $auteurId, $sujetId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $commentaire = new Commentaire();
        $commentaire->setContenu($contenu);
        $commentaire->setDateCom(new DateTime("now"));        
        $commentaire->setNote(0);

        $repositoryUtilisateur   = $this->getDoctrine()->getRepository(Utilisateur::class);
        $auteur  = $repositoryUtilisateur->find($auteurId);
        $commentaire->setAuteur($auteur);

        $repositorySujet   = $this->getDoctrine()->getRepository(Sujet::class);
        $sujet  = $repositorySujet->find($sujetId);
        $commentaire->setSujet($sujet);


        $entityManager->persist($commentaire);
        $entityManager->flush();

        $reponse = new Response(json_encode(array(
            'id'     => $commentaire->getId(),
            'contenu'    => base64_decode($commentaire->getContenu()),
            'date_com' => $commentaire->getDateCom()->format('Y-m-d H:i:s'),
            'note' => $sujet->getEtat()->getId(),
            'auteur' => array(
                        "id" => $commentaire->getAuteur()->getId(),
                        "pseudo" => $commentaire->getAuteur()->getPseudo(),
                    ),
            'sujet' => array(
                        "id" => $commentaire->getSujet()->getId(),
                        "titre" => $commentaire->getSujet()->getTitre(),
                    ),
        )));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/commentaire/delete/{id}", name="suppression_commentaire", methods={"DELETE"})
    */
    public function suppressionCommentaire($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Commentaire::class);
        $commentaire     = $repository->find($id);
        $entityManager->remove($commentaire);
        $entityManager->flush();
        $reponse = new Response(json_encode(array(
            'id'     => $commentaire->getId(),
            'auteur' => $commentaire->getAuteur()->getId(),
            ))
        );
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/commentaire/modif/{id}/{contenu}/{auteurId}/{sujetId}", name="modifier_commentaire", methods={"PUT"})
    */
    public function modifierCommentaire($id,$contenu, $auteurId, $sujetId)
    {
         $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Commentaire::class);
        $commentaire     = $repository->find($id);

        $commentaire->setContenu($contenu);
        $commentaire->setDateCom(new DateTime("now"));
        $commentaire->setNote(0);

        $repositoryUtilisateur   = $this->getDoctrine()->getRepository(Utilisateur::class);
        $auteur  = $repositoryUtilisateur->find($auteurId);
        $commentaire->setAuteur($auteur);

        $repositorySujet   = $this->getDoctrine()->getRepository(Sujet::class);
        $sujet  = $repositorySujet->find($sujetId);
        $commentaire->setAuteur($sujet);


        $entityManager->persist($commentaire);
        $entityManager->flush();

        $reponse = new Response(json_encode(array(
            'id'     => $commentaire->getId(),
            'contenu'    => base64_decode($commentaire->getContenu()),
            'date_com' => $commentaire->getDateCom()->format('Y-m-d H:i:s'),
            'note' => $sujet->getEtat()->getId(),
            'auteur' => array(
                        "id" => $commentaire->getAuteur()->getId(),
                        "pseudo" => $commentaire->getAuteur()->getPseudo(),
                    ),
            'sujet' => array(
                        "id" => $commentaire->getSujet()->getId(),
                        "titre" => $commentaire->getSujet()->getTitre(),
                    ),
        )));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }
}
