<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Sujet;
use App\Entity\Etat;
use App\Entity\Categorie;
use App\Entity\Utilisateur;
use App\Entity\Commentaire;
use \DateTime;

class CategorieController extends AbstractController
{
    /**
    * @Route("/categories", name="liste_categories", methods={"GET"})
    */
    public function listeCategories()
    {
        $repository   = $this->getDoctrine()->getRepository(Categorie::class);
        $listeCategories = $repository->findAll();
        $listeReponse = array();
        
        if($listeCategories) {

            foreach ($listeCategories as $categorie) {
                $listeReponse[] = array(
                    'id'     => $categorie->getId(),
                    'libelle'     => $categorie->getLibelle(),
                );
            }
        } else {
            $listeReponse[] = "Aucune catégorie ...";
        }

        $reponse = new Response();
        $reponse->setContent(json_encode(array("categories"=>$listeReponse)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/categorie/{id}", name="details_categorie", methods={"GET"})
    */
    public function detailsCategorie($id)
    {
        $repository   = $this->getDoctrine()->getRepository(Categorie::class);
        $categorie  = $repository->find($id);
                
        $reponse = new Response(json_encode(array(
            'id'     => $categorie->getId(),
            'libelle'     => $categorie->getLibelle(),
        )));
        
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/categorie/new/{libelle}", name="nouveau_categorie", methods={"POST"})
    */
    public function nouveauCategorie($libelle)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $categorie = new Categorie();
        $categorie->setLibelle($libelle);
        
        $entityManager->persist($categorie);
        $entityManager->flush();

        $reponse = new Response(json_encode(array(
            'id'     => $categorie->getId(),
            'libelle'    => $categorie->getLibelle(),            
        )));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/categorie/delete/{id}", name="suppression_commentaire", methods={"DELETE"})
    */
    public function suppressionCategorie($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Categorie::class);
        $categorie     = $repository->find($id);
        $entityManager->remove($categorie);
        $entityManager->flush();
        $reponse = new Response(json_encode(array(
            'id'     => $categorie->getId(),
            'libelle' => $categorie->getLibelle(),
            ))
        );
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/categorie/modif/{id}/{libelle}", name="modifier_categorie", methods={"PUT"})
    */
    public function modifierCommentaire($id,$libelle)
    {
         $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Categorie::class);
        $categorie     = $repository->find($id);

        $categorie->setLibelle($libelle);

        $entityManager->persist($categorie);
        $entityManager->flush();

        $reponse = new Response(json_encode(array(
            'id'     => $categorie->getId(),
            'libelle' => $categorie->getLibelle(),
            ))
        );

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }
}
