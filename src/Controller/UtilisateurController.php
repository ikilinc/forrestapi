<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Sujet;
use App\Entity\Utilisateur;
use App\Entity\Role;

class UtilisateurController extends AbstractController
{
    
    /**
    * @Route("/utilisateurs", name="liste_utilisateurs", methods={"GET"})
    */
    public function listeUtilisateurs()
    {
        $repository   = $this->getDoctrine()->getRepository(Utilisateur::class);
        $listeUtilisateur  = $repository->findAll();

        $listeReponse = array();
        $reponse = new Response();

        if($listeUtilisateur) {
            foreach ($listeUtilisateur as $utilisateur) {
                $listeReponse[] = array(
                    'id'     => $utilisateur->getId(),
                    'pseudo'    => $utilisateur->getPseudo(),
                    'mdp' => $utilisateur->getMdp(),
                    'mail' => $utilisateur->getMail(),
                    'role' => array(
                        'id' => $utilisateur->getRole()->getId(),
                        'libelle' => $utilisateur->getRole()->getLibelle(),
                    ),
                );
            }
        } else {
            $listeReponse[] = "Aucun utilisateur ...";
        }
        
        
        $reponse->setContent(json_encode(array("utilisateurs"=>$listeReponse)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/utilisateur/{id}", name="details_utilisateur", methods={"GET"})
    */
    public function detailsUtilisateur($id)
    {
        $repository = $this->getDoctrine()->getRepository(Utilisateur::class);
        $utilisateur     = $repository->find($id);

        $listeReponse = array();
        $reponse = new Response();

        if($utilisateur) {
            $listeReponse[] = array(
                'id'     => $utilisateur->getId(),
                'pseudo'    => $utilisateur->getPseudo(),
                'mail' => $utilisateur->getMail(),
                'role' => array(
                    'id' => $utilisateur->getRole()->getId(),
                    'libelle' => $utilisateur->getRole()->getLibelle(),
                ),
            );
        } else {
             $listeReponse = "Aucun utilisateur correspondant !";
        }

        $reponse->setContent(json_encode(array("utilisateur"=>$listeReponse)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    *ex : utilisateur/aWtpbGluYzA3QGdtYWlsLmNvbQ==/TGFsYWtlcnM3
    * @Route("/utilisateur/{mail}/{mdp}", name="verifier_authentification", methods={"GET"})
    */
    public function verifierAuthentification($mail, $mdp)
    {
        $repository   = $this->getDoctrine()->getRepository(Utilisateur::class);
        $utilisateur  = $repository->findOneBy(array('mail' => base64_decode($mail), 'mdp' => md5($mdp)));

        $reponse = new Response();

        if($utilisateur) {
            $listeReponse = array();

            $listeReponse[] = array(
                'id'     => $utilisateur->getId(),
                'pseudo'    => $utilisateur->getPseudo(),
                'mail' => $utilisateur->getMail(),
                'role' => array(
                    'id' => $utilisateur->getRole()->getId(),
                    'libelle' => $utilisateur->getRole()->getLibelle(),
                ),
            );

            $reponse->setContent(json_encode(array("utilisateur"=>$listeReponse))); 
        }
        else {
            $reponse->setContent(json_encode(array("message"=>"Utilisateur ou mot de passe incorrect !")));
        }
         
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

     /**
    * @Route("/utilisateur/new/{pseudo}/{mail}/{mdp}/{roleId}", name="nouveau_utilisateur", methods={"POST"})
    */
    public function nouveauUtilisateur($pseudo, $mail, $mdp, $roleId)
    {
        $repository   = $this->getDoctrine()->getRepository(Utilisateur::class);
        $pseudoExistant  = $repository->findOneBy(array('pseudo' => $pseudo));
        $mailExistant  = $repository->findOneBy(array('mail' => base64_decode($mail)));

        if(is_null($pseudoExistant) && is_null($mailExistant)) {
            $entityManager = $this->getDoctrine()->getManager();
            $utilisateur = new Utilisateur();
            $utilisateur->setPseudo($pseudo);
            $utilisateur->setMail(base64_decode($mail));
            $utilisateur->setMdp(md5($mdp));

            $repositoryRole   = $this->getDoctrine()->getRepository(Role::class);
            $role  = $repositoryRole->find($roleId);
            $utilisateur->setRole($role);

            $entityManager->persist($utilisateur);
            $entityManager->flush();

            $reponse = new Response(json_encode(array(
                    'id'     => $utilisateur->getId(),
                    'pseudo'    => $utilisateur->getPseudo(),
                    'mail' => $utilisateur->getMail(),
                    'role' => array(
                        'id' => $utilisateur->getRole()->getId(),
                        'libelle' => $utilisateur->getRole()->getLibelle(),
                    ),
                )
            ));
        } else {
            $reponse = new Response(json_encode(array(
                    'erreur'     => "pseudo ou e-mail existant",
                )
            ));
        }
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/utilisateur/delete/{id}", name="suppression_utilisateur", methods={"DELETE"})
    */
    public function suppressionUtilisateur($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Utilisateur::class);
        $utilisateur     = $repository->find($id);
        $entityManager->remove($utilisateur);
        $entityManager->flush();
        $reponse = new Response(json_encode(array(
                'id'     => $utilisateur->getId(),
                'pseudo'    => $utilisateur->getPseudo(),
                'mail' => $utilisateur->getMail(),
                'role' => array(
                            'id' => $utilisateur->getRole()->getId(),
                            'libelle' => $utilisateur->getRole()->getLibelle(),
                        ),
            ))
        );
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
    * @Route("/utilisateur/update/{id}/{pseudo}/{mail}/{mdp}/{roleId}", name="modification_utilisateur", methods={"PUT"})
    */
    public function modificationUtilisateur($id, $pseudo, $mail, $mdp, $roleId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Utilisateur::class);
        $utilisateur        = $repository->find($id);
        $utilisateur->setPseudo($pseudo);
        $utilisateur->setMail(base64_decode($mail));
        $utilisateur->setMdp(md5($mdp));

        $repositoryRole   = $this->getDoctrine()->getRepository(Role::class);
        $role  = $repositoryRole->find($roleId);
        $utilisateur->setRole($role);


        $entityManager->persist($utilisateur);
        $entityManager->flush();

        $reponse = new Response(json_encode(array(
            'id'     => $utilisateur->getId(),
            'pseudo'    => $utilisateur->getPseudo(),
            'mail' => $utilisateur->getMail(),
            'role' => array(
                            'id' => $utilisateur->getRole()->getId(),
                            'libelle' => $utilisateur->getRole()->getLibelle(),
                        ),
            )
        ));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }
}


