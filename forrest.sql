-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 20 jan. 2020 à 22:58
-- Version du serveur :  10.1.30-MariaDB
-- Version de PHP :  7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `forrest`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `libelle`) VALUES
(1, 'php'),
(2, 'javascript'),
(3, 'html5'),
(4, 'css3'),
(5, 'android');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE `commentaire` (
  `id` int(11) NOT NULL,
  `contenu` varchar(10000) NOT NULL,
  `date_com` datetime NOT NULL,
  `note` int(11) DEFAULT NULL,
  `auteur_id` int(11) NOT NULL,
  `sujet_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`id`, `contenu`, `date_com`, `note`, `auteur_id`, `sujet_id`) VALUES
(4, 'VHUgZG9pcyB1dGlsaXNlciBsYSBmb25jdGlvbiBzdHJfcmVwbGFjZQ==', '2020-01-16 13:14:45', 0, 2, 5),
(5, 'c3RyX3JlcGxhY2UoImJvbmpvdXIiLCJzYWx1dCIsJHZhcik=', '2020-01-16 13:59:59', 0, 3, 5),
(6, 'VHUgZG9pcyB1dGlsaXNlciBsYSBmb25jdGlvbiBjb3VudCgpIDogY291bnQoJGFycmF5KQ==', '2020-01-17 13:23:59', 0, 1, 6),
(14, 'YXpkZWY=', '2020-01-20 09:53:59', 0, 4, 5),
(18, 'dGVzdA==', '2020-01-20 14:36:54', 0, 4, 5);

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

CREATE TABLE `etat` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etat`
--

INSERT INTO `etat` (`id`, `libelle`) VALUES
(1, 'actif'),
(99, 'inactif');

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `libelle`) VALUES
(1, 'modérateur'),
(2, 'utilisateur');

-- --------------------------------------------------------

--
-- Structure de la table `sujet`
--

CREATE TABLE `sujet` (
  `id` int(11) NOT NULL,
  `titre` varchar(2048) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `date_creation` datetime NOT NULL,
  `etat_id` int(11) NOT NULL DEFAULT '1',
  `auteur_id` int(11) NOT NULL,
  `categorie_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `sujet`
--

INSERT INTO `sujet` (`id`, `titre`, `description`, `date_creation`, `etat_id`, `auteur_id`, `categorie_id`) VALUES
(5, 'UmVtcGxhY2VyIHVuZSB2YWxldXIgZGFucyB1biBzdHJpbmcgZW4gUEhQ', 'SidhaW1lcmFpcyByZW1wbGFjZXIgYm9uam91ciBwYXIgc2FsdXQgZGFucyBsYSB2YXJpYWJsZSBzdWl2YW50ZSA6ICR2YXIgPSAiYm9uam91ciB0b3V0IGxlIG1vbmRlIi4gQ29ubmFpc3Nlei12b3VzIGxhIGZvbmN0aW9uIHBvdXIgbWUgcGVybWV0dHJlIGNlY2kgc3ZwID8=', '2020-01-16 11:15:27', 1, 1, 1),
(6, 'Q29tcHRlciBsZSBub21icmUgZCfDqWzDqW1lbnRzIGRhbnMgdW4gYXJyYXkgZW4gUEhQ', 'SidhaSB1biBhcnJheSgnZnJ1aXRzJyA9PiAnYmFuYW5lJywgJ2xlZ3VtZXMnID0+ICdwb2lyZWF1eCcpOyBKJ2FpbWVyYWlzIHNhdm9pciBsZSBub21icmUgZCfDqWzDqW1lbnRzIGRhbnMgY2V0IGFycmF5IHN2cCAh', '2020-01-15 11:15:27', 1, 2, 1),
(7, 'Q3LDqWVyIHVuZSBiYWxpc2UgaHRtbCBlbiBKYXZhc2NyaXB0', 'TGUgdGl0cmUgcGFybGUgZGUgbHVpLW3Dqm1lLCBqZSBuJ3kgYXJyaXZlIHBhcy4uLg==', '2020-01-01 05:15:27', 1, 3, 2),
(21, 'Tm91dmVhdSBzdWpldCBlbiBodG1s', 'Tm91dmVhdSBzdWpldA==', '2020-01-20 15:50:55', 1, 4, 3);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(50) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `pseudo`, `mdp`, `mail`, `role_id`) VALUES
(1, 'isko', '5fa62ae6176f3746142503a6ebe96cb3', 'isko@forrest.fr', 1),
(2, 'patrak', '5fa62ae6176f3746142503a6ebe96cb3', 'patrak@forrest.fr', 1),
(3, 'jauwa', '5fa62ae6176f3746142503a6ebe96cb3', 'jauwa@forrest.fr', 1),
(4, 'utilisateur', '5fa62ae6176f3746142503a6ebe96cb3', 'utilisateur@forrest.fr', 2),
(5, 'roza', 'e985d748db7019cc451c00a0634dea41', 'roza@forrest.fr', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auteur` (`auteur_id`),
  ADD KEY `sujet` (`sujet_id`);

--
-- Index pour la table `etat`
--
ALTER TABLE `etat`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sujet`
--
ALTER TABLE `sujet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `etat` (`etat_id`),
  ADD KEY `auteur` (`auteur_id`),
  ADD KEY `categorie` (`categorie_id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`role_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `commentaire`
--
ALTER TABLE `commentaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `etat`
--
ALTER TABLE `etat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `sujet`
--
ALTER TABLE `sujet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `commentaire_ibfk_1` FOREIGN KEY (`auteur_id`) REFERENCES `utilisateur` (`id`),
  ADD CONSTRAINT `commentaire_ibfk_2` FOREIGN KEY (`sujet_id`) REFERENCES `sujet` (`id`);

--
-- Contraintes pour la table `sujet`
--
ALTER TABLE `sujet`
  ADD CONSTRAINT `sujet_ibfk_1` FOREIGN KEY (`etat_id`) REFERENCES `etat` (`id`),
  ADD CONSTRAINT `sujet_ibfk_2` FOREIGN KEY (`auteur_id`) REFERENCES `utilisateur` (`id`),
  ADD CONSTRAINT `sujet_ibfk_3` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
